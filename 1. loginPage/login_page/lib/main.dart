import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    //sections
    Widget emailSection = Container(
      padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 45.0),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: const Text("Email Address"),
                ),
                TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    hintText: "Enter Here"
                  ),
                ), 
              ],
            ),
          )
        ],
      ) 
    );

    Widget passwordSection = Container(
      padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: const Text("Password"),
                ),
                TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                      suffixIcon: const Align(
                        widthFactor: 3.0,
                        heightFactor: 1.0,
                        child: Icon(
                          Icons.visibility_off_outlined,
                        ),
                      ),
                    hintText: "Enter Here",
                  ),
                ), 
              ],
            ),
          ),
        ],
      ) 
    );

    Widget loginButton = Container(
      padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
      child: TextButton(
        style: OutlinedButton.styleFrom(
          primary: Colors.white,
          backgroundColor: const Color.fromARGB(125, 118, 170, 143),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10)
          ),
          minimumSize: const Size(400, 50),
        ),
        onPressed: (){},
        child: const Text(
          "Log in",
          style: TextStyle(fontSize: 18),),
        ),
    );

    Widget forgotSection = Container(
      padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
      child: Row(
        children: [
          const Text("Forgot password?"),
          Container(
            padding: const EdgeInsets.only(left: 5),
            child: const Text(
              "Click Here",
              style: TextStyle(
                color: Color.fromARGB(255, 89, 128, 108),
              ),
              ),
          ),
        ]
        ),
    );

    return MaterialApp(
      title: 'login page demo',
      theme: ThemeData(
        primaryColor: Colors.white,
        textTheme: const TextTheme(
          bodyMedium: TextStyle(fontSize: 16),
          titleMedium: TextStyle(color: Colors.grey),
        )
      ),
      home: Scaffold(
        appBar: AppBar(
          title: const Text(
            'Log In',
            style: TextStyle(color: Color.fromARGB(255, 87, 87, 87))
          ),
          backgroundColor: Colors.white,
        ),
        body: Column(
            children: [
              emailSection,
              passwordSection,
              loginButton,
              forgotSection
            ],  
        ),
      ),
    );
  }
}